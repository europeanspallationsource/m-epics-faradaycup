require ifcdaq,0.2.0
require mrfioc2,3.0.0
require pev,0.1.2

############################################################
############### configuration  DTACQ board #################
############################################################

# It is required to increase the channel access maximum size since large waveforms will be transferred.
epicsEnvSet EPICS_CA_MAX_ARRAY_BYTES 40000000

# Initialize the driver (Crate slot 1, FMC slot 2)
ndsCreateDevice(ifcdaq, CARD0, card=0, fmc=2)

############################################################
############### Initialisation EVG & EVR ###################
############################################################

epicsEnvSet("SYS"             "LNS-ISRC-010")
epicsEnvSet("EVG"             "EVG")
epicsEnvSet("EVR"             "EVR0")
epicsEnvSet("EVG_VMESLOT"     "2")
epicsEnvSet("EVR_PCIDOMAIN"   "0x0")
epicsEnvSet("EVR_PCIBUS"      "0x05")
epicsEnvSet("EVR_PCIDEVICE"   "0x0")
epicsEnvSet("EVR_PCIFUNCTION" "0x0")
epicsEnvSet("EVENT_14HZ"      "14")

mrmEvgSetupVME($(EVG), $(EVG_VMESLOT), 0x100000, 1, 0x01)
mrmEvrSetupPCI($(EVR), $(EVR_PCIDOMAIN), $(EVR_PCIBUS), $(EVR_PCIDEVICE), $(EVR_PCIFUNCTION))

dbLoadRecords("evg-vme-230.db", "DEVICE=$(EVG), SYS=$(SYS)")

dbLoadRecords("evr-pmc-230.db", "DEVICE=$(EVR), SYS=$(SYS)")

dbLoadRecords("evr-pulserMap.template", "DEVICE=$(EVR), SYS=$(SYS), EVT=$(EVENT_14HZ), PID=0, F=Trig, ID=0")

dbLoadRecords("FaradayCup.db")

#**********************************************************#
#*********************** INIT  ****************************#
#**********************************************************#

iocInit

############################################################
################### Configuration Timing ###################
############################################################

########## TIMING GENERATOR: setup TrgSrc0 to EVENT 14 ######
## External trigger from FrontInp0 - TrgSrc0
#dbpf $(SYS)-$(EVG):FrontInp0-TrigSrc0-SP $(EVENT_14HZ)

## Internal trigger from Mxc1 - TrgSrc0
dbpf $(SYS)-$(EVG):TrigEvt0-EvtCode-SP      $(EVENT_14HZ)
dbpf $(SYS)-$(EVG):Mxc1-Frequency-SP     1
dbpf $(SYS)-$(EVG):Mxc1-TrigSrc0-SP      "Set"

########## TIMING RECEIVER: setup OUT0 ##############
##set the pulser 0 to trigger on output 0
dbpf $(SYS)-$(EVR):FrontOut0-Src-SP 0
dbpf $(SYS)-$(EVR):Pul0-Evt-Trig0-SP $(EVENT_14HZ)
dbpf $(SYS)-$(EVR):Pul0-Width-SP 20000
dbpf $(SYS)-$(EVR):Pul0-Delay-SP 0
dbpf $(SYS)-$(EVR):FrontOut0-Ena-SP "Enabled"

############################################################
################### Configuration acquisition ##############
############################################################
dbpf $(SYS):CARD0:NSAMPLES 10
dbpf $(SYS):CARD0:SAMPLINGRATE 1000000
dbpf $(SYS):CARD0:TRIGGERSOURCE "EXT-GPIO"
sleep(1)
dbpf $(SYS):CARD0-STAT ON
sleep(3)
dbpf $(SYS):CARD0-STAT RUNNING
sleep(1)
dbpf $(SYS):CARD0-STAT RUNNING
